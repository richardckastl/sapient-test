document.addEventListener("DOMContentLoaded", function() {
  
  var formButton = document.getElementsByClassName('form__button')[0];
  var requiredInputs = document.getElementsByClassName('required');
  var successElement = document.getElementsByClassName('form__success')[0];

  formButton.onclick = function(e) {

  	var areAllFieldsComplete = true;

  	for(var i = 0; i < requiredInputs.length; i++) {
  		if (requiredInputs[i].value === "") {
  			areAllFieldsComplete = false;
  		}
  	}

  	if (areAllFieldsComplete === true) {
  		successElement.className += ' active';
  		e.preventDefault();
  	} else {
  		successElement.className = successElement.className.replace(' active', '');
  	}

  }

});