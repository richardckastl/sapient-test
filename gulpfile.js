/// <binding ProjectOpened='default' />

// *************************************
//
//   Gulpfile
//
// *************************************
//
// Available tasks:
//   `gulp sass`       (Compiles SASS, dependency of 'serve')
//   `gulp serve`      (Synced browser actions, also runs 'sass' [watch])
//
// *************************************
//
// -------------------------------------
//   Modules
// -------------------------------------
//
//   browser-sync      : Sync browser actions
//   css-mqpacker      : Combine all like media queries
//   es6-promise       : Requirement for gulp-sass
//   gulp              : The streaming build system
//   gulp-autoprefixer : Prefix CSS
//   gulp-imagemin     : Optimize images
//   gulp-load-plugins : Automatically load Gulp plugins
//   gulp-postcss      : Perform tasks on vanilla CSS
//   gulp-sass         : Compile Sass
//
// -------------------------------------

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var mqpacker = require('css-mqpacker');
var browserSync = require('browser-sync').create();
var outputPath = 'output';
var cleanCSS = require('gulp-cleancss');
require('es6-promise').polyfill();

var sassPaths = [
  'sass'
];

var proxy = "sapient-test.com.localhost";
var sassWatch = "sass/**/*.scss";

// -------------------------------------
//   Task: sass
//   Dependency of 'serve'
//   Default task
// -------------------------------------
gulp.task('sass', function() {
    var processors = [
    mqpacker
    ];
    return gulp.src('sass/master.scss')
    .pipe($.sass({
      includePaths: sassPaths
    })
    .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['>1%','last 20 versions', 'Safari 7']
    }))
    .pipe($.postcss(processors))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('css'));
});

// -------------------------------------
//   Task: serve
//   [watch]
// -------------------------------------
gulp.task('serve', ['sass'], function () {
    browserSync.init({
        proxy: proxy,
        open: false,
        notify: false,
        port: 3000
    });

    gulp.watch(sassWatch, ['sass']).on('change', browserSync.reload);
    gulp.watch('**/*.html').on('change', browserSync.reload);
});